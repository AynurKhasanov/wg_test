<?php

namespace app\controllers;

use Yii;
use app\models\Fibonacci;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;

class ApiController extends Controller
{
    public function actionIndex()
    {
        $model = new Fibonacci();
        $model->load(Yii::$app->request->get(), "");
        if ($model->validate()) {
            $model->getNumbers();
        } else {
            $message = '';
            foreach ($model->firstErrors as $key => $error) {
                $message .= $key . ':' . $error . ' ';
            }
            throw new BadRequestHttpException($message);
        }

        $result = [];
        foreach ($model->fibonacci_row as $key => $value) {
            $result[] = [
                'index' => $key,
                'value' => $value
            ];
        }
        return (object)$result;
    }
}