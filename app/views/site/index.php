<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\Fibonacci */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Тестирование';
?>
<div class="jumbotron">
    <?php $form = ActiveForm::begin([
        'id'     => 'fibonacci-form',
        'layout' => 'horizontal',
    ]); ?>

    <?= $form->field($model, 'start_index')->textInput() ?>

    <?= $form->field($model, 'end_index')->textInput() ?>

    <?= Html::submitButton('Посчитать', ['class' => 'btn btn-primary', 'name' => 'fibonacci-button']) ?>

    <?php ActiveForm::end(); ?>
</div>

<?= Html::a('Очистить кэш', ['clear-cache'], ['class' => 'btn btn-danger']) ?>

<?php if ($model->fibonacci_row) : ?>
    <h3>Результат</h3>
    <pre>
        <?php print_r($model->fibonacci_row); ?>
    </pre>
<?php endif; ?>
