<?php

namespace tests\unit\models;

use app\models\Fibonacci;

class FibonacciTest extends \Codeception\Test\Unit
{
    private $model;

    public function testGetNumbers()
    {
        $this->model = new Fibonacci([
            'start_index' => '1',
            'end_index'   => '6',
        ]);

        $this->model->validate();

        expect_that($this->model->getNumbers());
        expect_not($this->model->errors);
    }

    public function testFibonacciRow()
    {
        $this->model = new Fibonacci([
            'start_index' => '1',
            'end_index'   => '6',
        ]);

        $this->model->validate();

        $this->model->getNumbers();

        $check_array = [
            1 => 1,
            2 => 1,
            3 => 2,
            4 => 3,
            5 => 5,
            6 => 8,
        ];

        $this->assertEquals($check_array, $this->model->fibonacci_row);
    }

}