<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Fibonacci extends Model
{
    public $start_index;
    public $end_index;
    public $fibonacci_row;

    public function rules()
    {
        return [
            [['start_index', 'end_index'], 'required'],
            [['start_index', 'end_index'], 'trim'],
            [['start_index', 'end_index'], 'integer'],
            [['start_index', 'end_index'], 'safe'],
            [['start_index', 'end_index'], 'compare', 'compareValue' => 0, 'operator' => '>=', 'type' => 'number'],
            ['end_index', 'compare', 'compareAttribute' => 'start_index', 'operator' => '>=', 'type' => 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'start_index' => 'Индекс первого элемента',
            'end_index'   => 'Индекс последнего элемента',
        ];
    }

    public static function calculate($n)
    {
        if (!Yii::$app->redis->get($n)) {
            if ($n == 0) {
                Yii::$app->redis->set($n, 0);
            } else {
                if ($n == 1 || $n == 2) {
                    Yii::$app->redis->set($n, 1);
                } else {
                    $res = bcadd(self::calculate($n - 1), self::calculate($n - 2));
                    Yii::$app->redis->set($n, $res);
                }
            }
        }

        return Yii::$app->redis->get($n);
    }


    public function getNumbers()
    {
        for ($n = $this->start_index; $n <= $this->end_index; $n++) {
            $this->fibonacci_row[$n] = self::calculate($n);
        }
        return true;
    }
}