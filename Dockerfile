FROM php:7.2.6-fpm

MAINTAINER Khasanov Aynur <xacahob@bk.ru>

COPY init.sh /init.sh

WORKDIR '/code'

RUN apt-get update && apt-get install -y  \
    libpq-dev \
    curl \
    git \
    unzip \
    && docker-php-ext-install pdo pdo_pgsql bcmath \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod 755 /init.sh

ENV COMPOSER_ALLOW_SUPERUSER=1

ENTRYPOINT ["/init.sh"]

CMD ["init"]
